# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'atelie_layout/version'

Gem::Specification.new do |spec|
  spec.name          = "atelie-layout"
  spec.version       = AtelieLayout::VERSION
  spec.authors       = ["Herbertt Pavani Bamonde"]
  spec.email         = ["bamonde@webgoal.com.br"]
  spec.summary       = %q{Biblioteca com layouts padrões do Atêlie de Software.}
  spec.description   = %q{Definição dos layouts para utilizar nos projetos do Atêlie com o propósito de facilitar o desenvolvimento.}
  spec.homepage      = "http://atelie.software/"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "minitest"
  spec.add_development_dependency "rails"
end