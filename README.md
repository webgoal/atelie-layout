# Atelie::Layout

Gem para padronização do projetos do Atêlie.

## Installation

Add this line to your application's Gemfile:

    gem 'atelie-layout'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install atelie-layout

## Usage

Para gerar os templates:
    $ rails generate atelie_layout:install

## Contributing

1. Fork it ( http://github.com/<my-github-username>/atelie-layout/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
