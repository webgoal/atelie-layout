class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected

  def page_to_fetch
    params[:page] || 1
  end

  def pages_per_fetch
    params[:per_page] || 50
  end
end
