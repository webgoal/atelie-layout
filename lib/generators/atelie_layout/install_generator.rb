module AtelieLayout

  class InstallGenerator < Rails::Generators::Base
    desc "Sobrescrever as views defaults do Rails."

    source_root File.expand_path("../templates", __FILE__)

    def copy_layouts
      dir_entries("../templates/layouts") do |file_name|
        copy_file "layouts/#{file_name}", "app/views/layouts/#{file_name}", force: true
      end
    end

    def copy_views
      dir_entries("../templates/views") do |file_name|
        copy_file "views/#{file_name}", "lib/templates/erb/scaffold/#{file_name}", force: true
      end
      copy_file "views/home/index.html.erb", "app/views/home/index.html.erb"
    end

    def copy_assets_images
      copy_file "assets/images/logo.svg", "app/assets/images/logo.svg", force: true
    end

    def copy_assets_javascripts
      dir_entries("../templates/assets/javascripts") do |file_name|
        copy_file "assets/javascripts/#{file_name}", "app/assets/javascripts/#{file_name}", force: true
      end
    end

    def copy_assets_stylesheets
      dir_entries("../templates/assets/stylesheets") do |file_name|
        copy_file "assets/stylesheets/#{file_name}", "app/assets/stylesheets/#{file_name}", force: true
      end
    end

    def copy_locales
      dir_entries("../templates/locales") do |file_name|
        copy_file "locales/#{file_name}", "config/locales/#{file_name}"
      end
    end

    def remove_default_stylesheets
      remove_file "app/assets/stylesheets/application.css"
    end

    def copy_controllers
      copy_file "rails/scaffold_controller/controller.rb", "lib/templates/rails/scaffold_controller/controller.rb"
      dir_entries("../templates/controllers") do |file_name|
        copy_file "controllers/#{file_name}", "app/controllers/#{file_name}"
      end
    end

    def set_config
      insert_into_file "config/routes.rb", "\n  root \"home#index\"\n", after: ".routes.draw do\n"
    end

    def set_locale
      insert_into_file "config/application.rb", "    config.i18n.default_locale = :'pt-BR'\n", before: "  end\n"
    end

    private

    def dir_entries(folder_name)
      folder_path = "#{File.expand_path("#{folder_name}", __FILE__)}"
      Dir.entries(folder_path).each do |file_name|
        next unless File.file?(folder_path + '/' + file_name)
        yield file_name
      end
    end

  end
end
