require File.join(File.dirname(__FILE__), 'test_helper')
require 'rails/generators'
require 'atelie_layout'
require File.expand_path('../lib/generators/atelie_layout/install_generator', File.dirname(__FILE__))

class InstallGeneratorTest < Rails::Generators::TestCase
  tests AtelieLayout::InstallGenerator
  destination File.expand_path("../tmp", File.dirname(__FILE__))
  setup :prepare_destination, :create_routes_files

  def create_routes_files
    path = File.expand_path("../tmp/config", File.dirname(__FILE__))
    Dir.mkdir path
    file = File.new("#{path}/routes.rb", 'w+')
    file.write("Rails.application.routes.draw do\nend\n")
    file.close
  end

  # Layout
  test "create the layouts" do
    run_generator
    assert_file "app/views/layouts/application.html.erb"
  end

  # Views
  test "create the views" do
    run_generator
    assert_file "lib/templates/erb/scaffold/edit.html.erb"
    assert_file "lib/templates/erb/scaffold/index.html.erb"
    assert_file "lib/templates/erb/scaffold/new.html.erb"
    assert_file "lib/templates/erb/scaffold/show.html.erb"
    assert_file "lib/templates/erb/scaffold/_form.html.erb"
  end

  # Images
  test "create the images" do
    run_generator
    assert_file "app/assets/images/logo.svg"
  end

  # Javascripts
  test "create the javascripts" do
    run_generator
    assert_file "app/assets/javascripts/application.js"
  end

  # Stylesheets
  test "create the stylesheets" do
    run_generator
    assert_file "app/assets/stylesheets/_0-normalize.scss"
    assert_file "app/assets/stylesheets/_1-settings.scss"
    assert_file "app/assets/stylesheets/_custom-classes.scss"
    assert_file "app/assets/stylesheets/_header.scss"
    assert_file "app/assets/stylesheets/_sidebar.scss"
    assert_file "app/assets/stylesheets/_messages.scss"
    assert_file "app/assets/stylesheets/_login.scss"
    assert_file "app/assets/stylesheets/_override.scss"
    assert_file "app/assets/stylesheets/application.scss"
  end

  # Locales
  test "create the locales" do
    run_generator
    assert_file "config/locales/application.pt-BR.yml"
    assert_file "config/locales/defaults.pt-BR.yml"
    assert_file "config/locales/devise.pt-BR.yml"
    assert_file "config/locales/simple_form.pt-BR.yml"
  end

  # Controller Scaffold Template
  test "create the controller scaffold template" do
    run_generator
    assert_file "lib/templates/rails/scaffold_controller/controller.rb"
  end

  # Home Controller
  test "create the home controller" do
    run_generator
    assert_file "app/controllers/application_controller.rb"
    assert_file "app/controllers/home_controller.rb"
    assert_file "app/views/home/index.html.erb"
    assert_file "config/routes.rb", /Rails.application.routes.draw do\n\n  root \"home#index\"\nend\n/
  end

end
